#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

#include <cstddef> // size_t
#include <cstring> // strlen, strcpy

struct Cls
{
    Cls(char c, double d, int i) : c(c), d(d), i(i)
    {}
private:
    char c;
    double d;
    int i;
};

// Эта функция должна предоставить доступ к полю c объекта cls.
// Обратите внимание, что возвращается ссылка на char, т. е.
// доступ предоставляется на чтение и запись.
char &get_c(Cls &cls)
{
    // size_t max_size = max(max(sizeof(char), sizeof(double)), sizeof(int));
    size_t max_size = sizeof(char) > sizeof(double) ?
        (sizeof(char) > sizeof(int) ? sizeof(char) : sizeof(int))
        : (sizeof(double) > sizeof(int) ? sizeof(double) : sizeof(int));
    return *(char*)((char*)(&cls) + max_size * 0);
}

// Эта функция должна предоставить доступ к полю d объекта cls.
// Обратите внимание, что возвращается ссылка на double, т. е.
// доступ предоставляется на чтение и запись.
double &get_d(Cls &cls)
{
    // size_t max_size = max(max(sizeof(char), sizeof(double)), sizeof(int));
    size_t max_size = sizeof(char) > sizeof(double) ?
        (sizeof(char) > sizeof(int) ? sizeof(char) : sizeof(int))
        : (sizeof(double) > sizeof(int) ? sizeof(double) : sizeof(int));
    return *(double*)((char*)(&cls) + max_size * 1);
}

// Эта функция должна предоставить доступ к полю i объекта cls.
// Обратите внимание, что возвращается ссылка на int, т. е.
// доступ предоставляется на чтение и запись.
int &get_i(Cls &cls)
{
    size_t max_size = sizeof(char) > sizeof(double) ?
        (sizeof(char) > sizeof(int) ? sizeof(char) : sizeof(int))
        : (sizeof(double) > sizeof(int) ? sizeof(double) : sizeof(int));
    return *(int*)((char*)(&cls) + max_size * 2);
}

int main()
{
    Cls a('a', 1., 2);

    cout << get_c(a) << " " << get_d(a) << " " << get_i(a) << endl;
    
    return EXIT_SUCCESS;
}
