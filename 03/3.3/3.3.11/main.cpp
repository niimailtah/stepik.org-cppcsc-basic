#include <iostream>

using namespace std;

#include <cstddef> // size_t
#include <cstring> // strlen, strcpy

struct String
{
    explicit String(const char *str = "")
    {
        size = strlen(str);
        this->str = new char[size + 1];
        strcpy(this->str, str);
    }

    String(size_t n, char c)
    {
        size = n;
        this->str = new char[size + 1];
        memset(str, (int)c, size);
    }

    ~String()
    {
        delete[] str;
    }

    size_t size;
    char *str;
};

int main()
{
    String str("123");
    String uhty(10, 'u');
    
    return EXIT_SUCCESS;
}
