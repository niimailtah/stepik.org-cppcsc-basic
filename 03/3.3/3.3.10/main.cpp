#include <iostream>

using namespace std;

#include <cstddef> // size_t
#include <cstring> // strlen, strcpy

struct String
{
    explicit String(const char *str = "")
    {
        size = strlen(str);
        this->str = new char[size + 1];
        strcpy(this->str, str);
    }

    size_t size;
    char *str;
};

int main()
{
    String str("123");
    
    return EXIT_SUCCESS;
}
