#include <iostream>
#include <string>

using namespace std;

#include <cstddef> // size_t
#include <cstring> // strlen, strcpy

struct String
{
    explicit String(const char *str = "")
    {
        size = strlen(str);
        this->str = new char[size + 1];
        strcpy(this->str, str);
    }

    String(size_t n, char c)
    {
        size = n;
        this->str = new char[size + 1];
        memset(str, (int)c, size);
    }

    ~String()
    {
        delete[] str;
    }

    void append_cpp_style(String &other)
    {
        char *new_ptr;
        unsigned new_size;

        new_size = size + other.size + 1;
        new_ptr = new char[new_size];
        for (unsigned i = 0; i < size; ++i)
        {
            new_ptr[i] = str[i];
        }
        for (unsigned i = size; i < new_size; ++i)
        {
            new_ptr[i] = other.str[i - size];
        }
        delete [] str;
        size = new_size;
        str = new_ptr;
    }

    void append(String &other)
    {
        char *tempStr;
        unsigned new_size;

        new_size = size + other.size + 1;
        tempStr = new char[new_size];
        strcpy(tempStr, str);
        strcat(tempStr, other.str);
        delete [] str;
        size = new_size - 1;
        str = tempStr;
    }

    size_t size;
    char *str;
};

int main()
{
    const size_t ntest = 10;
    
    std::string tests[ntest][2] =
    {
        {"", ""},
        {"", "test"},
        {"test", ""},
        {"test", "test"},
        {"Hello ", " world!"},
        {"Supercalifragilistic", "expialidocious"}
    };
    
    for (size_t i = 0; i < ntest; ++i)
    {
        String t1(tests[i][0].c_str());
        String t2(tests[i][1].c_str());
        
        t1.append(t2);
        
        string res(t1.str);
        if (res != tests[i][0]+tests[i][1])
        {
            cout << "Test " << i + 1 << " failed!" << endl;
            cout << "Must be " << "\"" << tests[i][0] + tests[i][1] << "\"" << endl;
            cout << "But result is " << "\"" << res << "\"" << endl;
        }
        else
        {
            cout << "Test " << (i + 1) << " passed!" << endl;
        }
        
    }
    
    string last = "Same pointer test";
    String t(last.c_str());
    t.append(t);
    
    std::string res(t.str);
    
    if (res != last + last)
    {
        cout << "Test " << ntest + 1 << " failed!" << endl;
        cout << "Must be " << last + last << endl;
        cout << "But result is " << res << endl;
    }
    else
    {
        cout << "Test " << ntest + 1 << " passed!" << endl;
    }

    return EXIT_SUCCESS;
}
