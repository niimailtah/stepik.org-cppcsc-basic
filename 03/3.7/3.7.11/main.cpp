#include <iostream>

using namespace std;

#include <cstddef> // size_t
#include <cstring> // strlen, strcpy

struct String
{
    String(const char *str = "");
    String(size_t n, char c);
    ~String();

    /* Реализуйте конструктор копирования */
    String(const String &other)
        : size(other.size)
    {
        str = new char[other.size + 1];
        strcpy(str, other.str);
    }

    // Воспользуемся материалами лекции
    void swap(String &other)
    {
        std :: swap(this -> size, other.size);
        std :: swap(this -> str, other.str);
    }
        
    /* И действительно реализовываем оператор присваивания */
    String &operator=(const String &other)
    {
        if (this != &other)
        {
            String(other).swap(*this);
        }
        return *this;
    }

    void append(const String &other);

    size_t size;
    char *str;
};

int main()
{
    return EXIT_SUCCESS;
}
