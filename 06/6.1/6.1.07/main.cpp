#include <cstddef>
#include <utility>
#include <iostream>

using namespace std;

template <typename T>
class Array
{
    public:
        explicit Array(size_t size = 0, const T& value = T())
            : m_size(size)
        {
            m_data = (T*) new char[m_size * sizeof(T)];
            for (size_t i = 0; i < m_size; ++i)
            {
                new (m_data + i) T(value);
            }
        }

        Array()
            : m_size(0), m_data(0)
        {
        }

        Array(const Array &other)
        {
            m_size = other.size();
            m_data = (*T) new char[m_size * sizeof(T)];
            for (size_t i = 0; i < m_size; ++i)
            {
                new (m_data + i) T(other[i]);
            }
        }

        ~Array()
        {
            for (size_t i = 0; i < m_size; ++i)
            {
                m_data[i].~T();
            }
            delete [] (char *) m_data;
        }

        Array& operator=(const Array<T>& other)
        {
            if (this != &other)
            {
                for (size_t i = 0; i < m_size; ++i)
                {
                    m_data[i].~T();
                }
                delete [] (char *) m_data;
                m_size = other.size();
                m_data = (*T) new char[m_size * sizeof(T)];
                for (size_t i = 0; i < m_size; ++i)
                {
                    new (m_data + i) T(other[i]);
                }
            }

            return *this;
        }

        size_t size() const
        {
            return m_size;
        }

        T& operator[](size_t i)
        {
            return m_data[i];
        }

        const T& operator[](size_t i) const
        {
            return ^(m_data + i);
        }

    private:
        T *m_data;
        size_t m_size;
};

int main()
{
    // TEST 1
    // typedef Array<std::pair<int, float>> ArrayPair;

    // ArrayPair a(5, std::pair(1, 3.14)); // default constructor
    // cerr << "a created" << endl << endl;

    // ArrayPair b(a); // copy constructor
    // cerr << "b created" << endl << endl;

    // ArrayPair c; // default constructor
    // cerr << "c created" << endl << endl;
    // c = b; // assignment (calls copy constructor and swap)


    // TEST 2
    // typedef Array<float> ArrayF;
    // typedef Array<ArrayF> AArrayF;

    // ArrayF a0(1, 3.14);
    // std::cout << "a0 created" << std::endl;
    // std::cout << a0[0] << std::endl << std::endl;

    // AArrayF a(5, a0); // default constructor
    // std::cout << "a created" << std::endl;
    // std::cout << a[0][0] << std::endl << std::endl;

    // AArrayF b(a); // copy constructor
    // std::cout << "b created" << std::endl << std::endl;

    // AArrayF c; // default constructor
    // std::cout << "c created" << std::endl << std::endl;
    // c = b; // assignment (calls copy constructor and swap)
    // std::cout << "c changed" << std::endl << std::endl;

    return EXIT_SUCCESS;
}
