#include <cstddef>
#include <iostream>
#include <vector>

using namespace std;

template <typename T>
class Array
{
    public:
        explicit Array(size_t size = 0, const T& value = T())
        {
            mData = (T*) new char[mSize * sizeof(T)];
            for (size_t i = 0; i < mSize; ++i)
            {
                new(mData + i) T(value);
            }
        }

        Array() : mSize(0), mData(0)
        {
        };

        Array(const Array& other)
        {
            mSize = otherArray.size();
            mData = (T*) new char[mSize * sizeof(T)];
            for (size_t i = 0; i < mSize; ++i)
            {
                new(mData + i) T(otherArray[i]);
            }
        }

        ~Array()
        {
            for (size_t i = 0; i != mSize; ++i)
            {
                mData[i].~T();
            }
            delete[] (char*) mData;
        }

        Array& operator=(Array other)
        {
            if (this != &otherArray)
            {
                for (size_t i = 0; i < mSize; i++)
                {
                    mData[i].~T();
                }
                delete[] (char*) mData;
                mSize = otherArray.size();
                mData = (T*) new char[mSize * sizeof(T)];
                for (size_t i = 0; i < mSize; ++i)
                {
                    new(mData + i) T(otherArray[i]);
                }
            }
            return *this;
        }

        // void swap(Array &other);
        size_t size() const
        {
            return mSize;
        }

        T& operator[](size_t index)
        {
            return mData[index];
        }

        const T& operator[](size_t index) const
        {
            return *(mData + index);
        }

    private:
        size_t mSize;
        T *mData;
};

bool less(int a, int b)
{
    return a < b;
}

struct Greater
{
    bool operator()(int a, int b) const
    {
        return b < a;
    }
};

void show(const char* name, vector <double> & v)
{
    cout << "Vector '" << name << "'" << endl;
    for(auto & item : v)
    {
        cout << item << " ";
    }
    cout << endl;
}

template <typename TSource, class Comparator>
TSource minimum(Array<TSource>& array, Comparator cmp)
{
    size_t res = 0;
    for (size_t i = 1; i < array.size(); ++i)
    {
        if (cmp(array[i], array[res]))
        {
            res = i;
        }
    }
    
    return array[res];
}

int main()
{
    Array<int> ints(3);
    ints[0] = 10;
    ints[1] = 2;
    ints[2] = 15;
    int min = minimum(ints, ::less);    // в min должно попасть число 2
    int max = minimum(ints, Greater()); // в max должно попасть число 15

    return EXIT_SUCCESS;
}
