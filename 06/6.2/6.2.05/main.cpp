#include <iostream>
#include <vector>

using namespace std;

void show(const char* name, vector <double> & v)
{
    cout << "Vector '" << name << "'" << endl;
    for(auto & item : v)
    {
        cout << item << " ";
    }
    cout << endl;
}

template <typename TSource, typename TDest>
void copy_n(TDest *d, TSource *s, size_t n)
{
    for (size_t i = 0; i < n; i++)
    {
        d[i] = TDest(s[i]);
    }
}

int main()
{
    int ints[] = {1, 2, 3, 4};
    double doubles[4] = {};

    ::copy_n(doubles, ints, 4); // теперь в массиве doubles содержатся элементы 1.0, 2.0, 3.0 и 4.0

    return EXIT_SUCCESS;
}
