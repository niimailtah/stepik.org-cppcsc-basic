#include <iostream>

using namespace std;

// определите только функцию power, где
//    x - число, которое нужно возвести в степень
//    p - степень, в которую нужно возвести x
//

int power(int x, unsigned p)
{
    int answer;
    
    /* считаем answer */
    answer = 1;
    for (int current_powwer = 0; current_powwer < p; ++current_powwer)
    {
        answer *= x;
    }

    return answer;
}

int main()
{
    cout << power(2, 2) << endl;

    return EXIT_SUCCESS;
}
