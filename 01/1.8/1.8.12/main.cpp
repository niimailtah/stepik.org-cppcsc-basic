#include <iostream>

using namespace std;

int main()
{
    char c = '\0', prev_c = '\0';

    while (cin.get(c))
    {
        if (c == ' ' && prev_c == ' ')
        {
            continue;
        }
        cout << c;
        prev_c = c;
    }

    return EXIT_SUCCESS;
}
