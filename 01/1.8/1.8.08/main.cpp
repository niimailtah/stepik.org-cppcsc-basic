#include <iostream>

using namespace std;

/* присваивает r максимум из x и y */
#define MAX(x, y, r) {int x_ = (x); int y_ = (y); int r_ = (x_ > y_ ? x_ : y_); r = r_;}

int main()
{
    int a = 10;
    int b = 20;
    int c = 0;

    MAX(a, b, c);
    cout << c << endl;

    MAX(a += b, b, c);
    cout << c << endl;

    return EXIT_SUCCESS;
}
