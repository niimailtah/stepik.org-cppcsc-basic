#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int a, b, c;
    double D, x1, x2;

    cin >> a >> b >> c;
    D = b * b - 4 * a * c;

    if (D < 0)
    {
        cout << "No real roots" << endl;
        return EXIT_SUCCESS;
    }
    x1 = (-b + sqrt(D)) / (2 * a);
    x2 = (-b - sqrt(D)) / (2 * a);
    cout << x1 << " " << x2 << endl;

    return EXIT_SUCCESS;
}
