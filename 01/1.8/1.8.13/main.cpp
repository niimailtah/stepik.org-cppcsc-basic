#include <iostream>

using namespace std;

int log_2(int a)
{
    int p, current;

    p = 0;
    current = 1;
    while (current <= a)
    {
        current *= 2;
        p++;
    }
    return p - 1;
}

int main()
{
    int t, a;

    cin >> t;
    for (int i = 0; i < t; ++i)
    {
        cin >> a;
        cout << log_2(a) << endl;
    }
    return EXIT_SUCCESS;
}
