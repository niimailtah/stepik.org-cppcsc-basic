#include <iostream>

using namespace std;

// У интерфейсов нет таблиц виртуальных методов. ?
struct IInterface
{
    virtual int pure_virtual();
};

class Base
{
    public:
        virtual void public_method();
    private:
        virtual void private_metod();
        // Чистый виртуальный метод с определением, см. определение ниже
        virtual void pure_virtual_method() = 0;
        // обычный виртуальный метод
        // virtual void pure_virtual_method();
        int private_int;
    protected:
        void protected_method();
};

// Чистый виртуальный метод с определением
void Base::pure_virtual_method()
{
    cout << "void Base::pure_virtual_method()" << endl;
}

class Derived : Base
{
    private:
        // Производные классы не могут переопределять private-виртуальные методы
        // базового класса, если они унаследованы от базового класса с модификатором
        // private.
        // false
        void private_metod()
        {
            // Производные классы не видят private-предков своего базового класса.
            // true
            // private_int = 1;
        }
        // Если в базовом классе виртуальная функция определена как public,
        // то в производном классе её можно переопределить как private.
        // true
        virtual void public_method();
};

class AnotherDerived : Base
{
    public:
        // Если в базовом классе виртуальная функция определена как private,
        // то в производном классе её можно переопределить как public.
        // true
        virtual void private_metod();
};

class PrivateDerived : private Base
{
    public:
        // Производные классы не видят protected-предков своего (непосредственного)
        // базового класса, если они унаследованы от базового класса
        // с модификатором private.
        // false
        void try_to_view()
        {
            protected_method();
        }
};

int main()
{
    return EXIT_SUCCESS;
}
