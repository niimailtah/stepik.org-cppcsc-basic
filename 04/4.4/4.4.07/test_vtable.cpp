#include <iostream>
#include <iomanip>
#include <cstring>

using namespace std;

class Base
{
    public:
        int i;
        virtual void method()
        {
            cout << "Base" << endl;
        }

        virtual void method2()
        {
            cout << "Base2" << endl;
        }

        virtual void method3()
        {
            cout << "Base3" << endl;
        }

        virtual void method4()
        {
            cout << "Base4" << endl;
        }
};

class Delivered : public Base
{
    public:
        long j;
        virtual void  method()
        {
            cout << "Delivered" << endl;
        }

        virtual void method2()
        {
            cout << "Delivered2" << endl;
        }

        virtual void method3()
        {
            cout << "Delivered3" << endl;
        }

        virtual void method4()
        {
            cout << "Delivered4" << endl;
        }
};


void print_data(void * data, int size, int bytes_per_line = 8)
{
    unsigned char * data_ = (unsigned char*)data;
    for (int i = 0; i < size; ++i)
    {
        if (i % bytes_per_line == 0)
        {
            cout << endl << i << " | ";
        }
        cout << setfill('0') << setw(2) << hex << (int)data_[i] << " ";
    }
    cout << endl;
}

void * get_vtable_address(void * object)
{
    void * vtable;
    std::memcpy(&vtable, object, 8);
    return vtable;
}

int main()
{

    Base * b = new Base();
    Base * bd = new Delivered();
    Delivered * d = new Delivered();

    cout << "-----/ sizes /-----" << endl;
    cout << "sizeof(Base) = " << sizeof(Base) << ", sizeof(*b) = " << sizeof(*b) << endl;
    cout << "sizeof(Delivered) = " << sizeof(Delivered) << ", sizeof(*bd) = " << sizeof(*bd) << endl;
    cout << "sizeof(Delivered) = " << sizeof(Delivered) << ", sizeof(*d) = " << sizeof(*d) << endl;

    b->method();
    bd->method();
    d->method();

    std::cout << "-----/ b /-----";
    print_data(b, sizeof(*b));

    std::cout << "-----/ bd /-----";
    print_data(bd, sizeof(*bd));

    std::cout << "-----/ d /-----";
    print_data(d, sizeof(*d));

    cout << "vtable b = " << get_vtable_address(b) << endl;
    cout << "vtable bd = " << get_vtable_address(bd) << endl;

    // Try call b virtual method
    void * b_vtable = get_vtable_address(b);

    typedef void (*void_func)();

    void_func b_method = ((void_func*)b_vtable)[0];
    void_func b2_method = ((void_func*)b_vtable)[1];
    void_func b3_method = ((void_func*)b_vtable)[2];
    void_func b4_method = ((void_func*)b_vtable)[3];

    b_method();
    b2_method();
    b3_method();
    b4_method();


    //Try call bd virtual methods
    void * bd_vtable = get_vtable_address(bd);

    typedef void (*void_func)();

    void_func bd_method = ((void_func*)bd_vtable)[0];
    void_func bd2_method = ((void_func*)bd_vtable)[1];
    void_func bd3_method = ((void_func*)bd_vtable)[2];
    void_func bd4_method = ((void_func*)bd_vtable)[3];

    bd_method();
    bd2_method();
    bd3_method();
    bd4_method();

    return EXIT_SUCCESS;
}
