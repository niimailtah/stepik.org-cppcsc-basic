#include <iostream>

using namespace std;

struct Expression
{
    virtual ~Expression() {};
    virtual double evaluate() const = 0;
};

struct Number : Expression
{
    Number(double value)
        : value(value)
    {}

    double evaluate() const
    {
        return value;
    }

private:
    double value;
};

struct BinaryOperation : Expression
{
    /*
      Здесь op это один из 4 символов: '+', '-', '*' или '/', соответствующих операциям,
      которые вам нужно реализовать.
     */
    BinaryOperation(Expression const * left, char op, Expression const * right)
        : left(left), op(op), right(right)
    {}

    ~BinaryOperation()
    {
        delete left;
        delete right;
    }

    double evaluate() const
    {
        double value;

        switch (op)
        {
            case '+':
                value = left->evaluate() + right->evaluate();
                break;
            case '-':
                value = left->evaluate() - right->evaluate();
                break;
            case '*':
                value = left->evaluate() * right->evaluate();
                break;
            case '/':
                value = left->evaluate() / right->evaluate();
                break;
        }
        return value;
    }

private:
    Expression const * left;
    Expression const * right;
    char op;
};

bool check_equals(Expression const *left, Expression const *right)
{
    return ((void(*(*(&)[2]))(const Expression&))left)[0][0] == ((void(*(*(&)[2]))(const Expression&))right)[0][0];
}

int main()
{
    Number * num1 = new Number(4.5);
    Number * num2 = new Number(5);
    Expression * expr = new BinaryOperation(num1, '*', num2);

    cout << check_equals(num1, num2) << endl;
    cout << check_equals(num1, expr) << endl;

    delete expr;
    return EXIT_SUCCESS;
}
