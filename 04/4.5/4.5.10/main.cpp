#include <iostream>

using namespace std;

struct Number;
struct BinaryOperation;

struct Visitor
{
    virtual void visitNumber(Number const * number) = 0;
    virtual void visitBinaryOperation(BinaryOperation const * operation) = 0;
    virtual ~Visitor()
    {}
};

struct Expression
{
    virtual double evaluate() const = 0;
    virtual void visit(Visitor * vistitor) const = 0;
    virtual ~Expression()
    {}
};

struct Number : Expression
{
    Number(double value)
        : value(value)
    {}

    double evaluate() const
    {
        return value;
    }

    double get_value() const
    {
        return value;
    }

    void visit(Visitor * visitor) const
    {
        visitor->visitNumber(this);
    }

private:
    double value;
};

struct BinaryOperation : Expression
{
    BinaryOperation(Expression const * left, char op, Expression const * right)
        : left(left), op(op), right(right)
    {}

    ~BinaryOperation()
    {
        delete left;
        delete right;
    }

    double evaluate() const
    {
        double value;

        switch (op)
        {
            case '+':
                value = left->evaluate() + right->evaluate();
                break;
            case '-':
                value = left->evaluate() - right->evaluate();
                break;
            case '*':
                value = left->evaluate() * right->evaluate();
                break;
            case '/':
                value = left->evaluate() / right->evaluate();
                break;
        }
        return value;
    }

    Expression const * get_left() const
    {
        return left;
    }

    Expression const * get_right() const
    {
        return right;
    }

    char get_op() const
    {
        return op;
    }

    void visit(Visitor * visitor) const
    {
        visitor->visitBinaryOperation(this);
    }

private:
    Expression const * left;
    Expression const * right;
    char op;
};

struct PrintVisitor : Visitor
{
    void visitNumber(Number const * number)
    {
        cout << "(" << number->get_value() << ")";
    }

    void visitBinaryOperation(BinaryOperation const * bop)
    {
        cout << "(";
        bop->get_left()->visit(this);
        cout << " " << bop->get_op() << " ";
        bop->get_right()->visit(this);
        cout << ")";
    }
};

Expression const * get_expression(int n_expr)
{
    Expression const * expr;
    Expression * sube;
    Expression * num1 = new Number(1.1);
    Expression * num2 = new Number(2.2);
    Expression * num3 = new Number(3.3);

    switch (n_expr)
    {
        case 0:
            expr = new BinaryOperation(
                num1,
                '+',
                new BinaryOperation(
                    num2,
                    '*',
                    num3)
                );
            break;
        case 1:
            sube = new BinaryOperation(
                new Number(4.5),
                '+',
                new Number(5));
            expr = new BinaryOperation(
                new Number(3),
                '*',
                sube);
            break;
        default:
            expr = nullptr;
            break;
    }

    return expr;
}

int main()
{
    Expression const * expr;
    PrintVisitor visitor;

    expr = get_expression(0);
    expr->visit(&visitor);
    cout << endl;

    expr = get_expression(1);
    expr->visit(&visitor);
    cout << endl;

    return EXIT_SUCCESS;
}
