#include <iostream>

using namespace std;

#include <string> // std::string

struct Expression
{
    virtual ~Expression() {};
    virtual double evaluate() const = 0;
};

struct Number : Expression
{
    Number(double value)
        : value(value)
    {}

    double evaluate() const
    {
        return value;
    }

private:
    double value;
};

struct BinaryOperation : Expression
{
    /*
      Здесь op это один из 4 символов: '+', '-', '*' или '/', соответствующих операциям,
      которые вам нужно реализовать.
     */
    BinaryOperation(Expression const * left, char op, Expression const * right)
        : left(left), op(op), right(right)
    {}

    ~BinaryOperation()
    {
        delete left;
        delete right;
    }

    double evaluate() const
    {
        double value;

        switch (op)
        {
            case '+':
                value = left->evaluate() + right->evaluate();
                break;
            case '-':
                value = left->evaluate() - right->evaluate();
                break;
            case '*':
                value = left->evaluate() * right->evaluate();
                break;
            case '/':
                value = left->evaluate() / right->evaluate();
                break;
        }
        return value;
    }

private:
    Expression const * left;
    Expression const * right;
    char op;
};

int main()
{
    // сначала создаём объекты для подвыражения 4.5 * 5
    Expression * sube = new BinaryOperation(new Number(4.5), '*', new Number(5));
    // потом используем его в выражении для +
    Expression * expr = new BinaryOperation(new Number(3), '+', sube);

    // вычисляем и выводим результат: 25.5
    std::cout << expr->evaluate() << std::endl;

    // тут освобождаются *все* выделенные объекты
    // (например, sube будет правым операндом expr, поэтому его удалять не нужно)
    delete expr;
    return EXIT_SUCCESS;
}
