#include <iostream>

using namespace std;

namespace loc {
void strcat(char *to, const char *from)
{
    while (*to != '\0')
    {
        to++;
    }
    while (*from != '\0')
    {
        *to = *from;
        to++;
        from++;
    }
    *to = '\0';
    
    return;
}
} // namespace loc


int main()
{
    char hello[100] = {"Hello"};
    char *world = {" world!"};

    loc::strcat(hello, world);
    cout << hello << endl;

    return EXIT_SUCCESS;
}
