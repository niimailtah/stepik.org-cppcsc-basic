#include <iostream>

using namespace std;

namespace loc {
unsigned strlen(const char *str)
{
    int length = 0;

    while (str[length++] != '\0')
    {
    }

    return length - 1;
}
} // namespace loc

int main()
{
    char *hello = {"hello"};

    cout << loc::strlen(hello) << endl;

    return EXIT_SUCCESS;
}
