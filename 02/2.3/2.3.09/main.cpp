#include <iostream>

using namespace std;

void shift_left(int a[], unsigned size)
{
    int tmp = a[0];

    for (int i = 0; i < size - 1; i++)
    {
        a[i] = a[i + 1];
    }
    a[size - 1] = tmp;
}

void rotate(int a[], unsigned size, int shift)
{
    for (int i = 0; i < shift; i++)
    {
        shift_left(a, size);
    }
}

int main()
{
    int a[] = { 1, 2, 3, 4, 5 };
    unsigned int size = 5;
    int shift = 2;

    rotate(a, size, shift);
    // Вывод массива
    for (int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;

    return EXIT_SUCCESS;
}
