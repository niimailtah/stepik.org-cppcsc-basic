#include <iostream>

using namespace std;

namespace loc {
int ** transpose(const int * const * m, unsigned rows, unsigned cols);
void show(const int * const * m, unsigned rows, unsigned cols);
int ** create_array2d(size_t a, size_t b);
void free_array2d(int ** m, size_t a, size_t b);

int ** transpose(const int * const * m, unsigned rows, unsigned cols)
{
    int **t_m;
    size_t current_row, current_col;

    t_m = create_array2d(cols, rows);
    for (current_row = 0; current_row < rows; ++current_row)
    {
        for (current_col = 0; current_col < cols; ++ current_col)
        {
            t_m[current_col][current_row] = m[current_row][current_col];
        }
    }

    return t_m;
}

void show(const int * const * m, unsigned rows, unsigned cols)
{
    size_t current_row, current_col;

    for (current_row = 0; current_row < rows; ++current_row)
    {
        for (current_col = 0; current_col < cols; ++ current_col)
        {
            cout << m[current_row][current_col] << " ";
        }
        cout << endl;
    }
}

int ** create_array2d(size_t a, size_t b)
{
    int ** m = new int *[a];
    m[0] = new int[a * b];
    for (size_t i = 1; i != a; ++i)
    {
        m[i] = m[i - 1] + b;
    }
    return m;
}

void free_array2d(int ** m, size_t a, size_t b)
{
    delete [] m[0];
    delete [] m;
}

} // namespace loc

int main()
{
    //TODO: make tests
    return EXIT_SUCCESS;
}
