#include <iostream>
// используется для форматирования вывода матрицы
#include <iomanip>

using namespace std;

namespace loc {
int ** create_array2d(size_t a, size_t b)
{
    int ** m = new int *[a];
    for (size_t i = 0; i != a; ++i)
    {
        m[i] = new int[b];
    }
    return m;
}

void free_array2d(int ** m, size_t a, size_t b)
{
    for (size_t i = 0; i != a; ++i)
    {
        delete [] m[i];
    }
    delete [] m;
}

int ** create_array2d_effective(size_t a, size_t b)
{
    int ** m = new int *[a];
    m[0] = new int[a * b];
    for (size_t i = 1; i != a; ++i)
    {
        m[i] = m[i - 1] + b;
    }
    return m;
}

void free_array2d_effective(int ** m, size_t a, size_t b)
{
    delete [] m[0];
    delete [] m;
}

void read_array2d(int **m, size_t a, size_t b)
{
    for (size_t i = 0; i != a; ++i)
    {
        for (size_t j = 0; j != b; ++j)
        {
            std::cin >> m[i][j];
        }
    }
}

void print_array2d(int **m, size_t a, size_t b)
{
    for (size_t i = 0; i != a; ++i)
    {
        for (size_t j = 0; j != b; ++j)
        {
            std::cout << std::setw(4) << m[i][j];
        }
        std::cout << '\n';
    }
}

int min_row(const int * const * m, unsigned rows, unsigned cols)
{
    size_t current_row, current_col;
    int min = m[0][0];
    int min_row = 0;

    for (current_row = 0; current_row < rows; ++current_row)
    {
        for (current_col = 0; current_col < cols; ++ current_col)
        {
            if (min > m[current_row][current_col])
            {
                min = m[current_row][current_col];
                min_row = current_row;
            }
        }
    }

    return min_row;
}

void swap_min(int *m[], unsigned rows, unsigned cols)
{
    int r_min = min_row(m, rows, cols);

    swap(m[0], m[r_min]);
}
} // namespace loc

int main()
{
    // Перенаправление стаднартного ввода на файл input.txt
    freopen("input.txt", "r", stdin); // перенаправление потока ввода в стиле C

    size_t a, b;
    std::cin >> a >> b;
    int **m = loc::create_array2d_effective(a, b);
    loc::read_array2d(m, a, b);
    cout << std::setw(2*b+8) << "Source matrix" << endl;
    loc::print_array2d(m, a, b);
    loc::swap_min(m, a, b);
    cout << std::setw(2*b+4) << "Result" << endl;
    loc::print_array2d(m, a, b);
    loc::free_array2d_effective(m, a, b);

    return EXIT_SUCCESS;
}
