#include <iostream>

using namespace std;

unsigned int cnt;

int foo(int n)
{
    cnt++;
    cout << cnt << ": foo(" << n << ")" << endl;
    if (n <= 0)
    {
        return 1;
    }
    return foo((n * 2) / 3) + foo(n - 2);
}

int main()
{
    cnt = 0;
    foo(3);
    cout << cnt << endl;

    return EXIT_SUCCESS;
}
