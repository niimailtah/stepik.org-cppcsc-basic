#include <iostream>

using namespace std;

void reverse(void)
{
    int current;

    cin >> current;
    if (current != 0)
    {
        reverse();
        cout << current << " ";
    }

    return;
}

int main()
{
    reverse();

    return EXIT_SUCCESS;
}
