Пекурсивную функция НОД
======================

Напишите рекурсивную функцию вычисления наибольшего общего делителя двух положительных целых чисел (Greatest Common Divisor, GCD). Для этого воспользуйтесь следующими свойствами:

1. $GCD(a, b) = GCD(b, a \bmod b)$
2. $GCD(0, a) = a$
3. $GCD(a, b) = GCD(b, a)$

**Требования к реализации:** в данном задании запрещено пользоваться циклами. Вы можете заводить любые вспомогательные функции, если они вам нужны. Функцию `main` определять не нужно.

---
## Алгоритм Евклида ##

![Алгоритм Евклида](./ae.png)
