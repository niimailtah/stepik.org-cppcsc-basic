#include <iostream>

using namespace std;

unsigned gcd(unsigned a, unsigned b)
{
    if (a != 0 && b != 0)
    {
        if (a > b)
        {
            return gcd(a % b, b);
        }
        return gcd(a, b % a);
    }
    return (a + b);
}


int main()
{
    unsigned int a, b;

    cin >> a >> b;
    cout << gcd(a, b) << endl;

    return EXIT_SUCCESS;
}
