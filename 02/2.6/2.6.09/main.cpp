#include <iostream>

using namespace std;

namespace loc {
char *resize(const char *str, unsigned size, unsigned new_size)
{
    char *new_ptr;

    new_ptr = new char[new_size];
    for (unsigned i = 0; i < new_size; ++i)
    {
        new_ptr[i] = str[i];
    }
    delete [] str;

    return new_ptr;
}
} // namespace loc

int main()
{
    size_t size, new_size;
    char *ptr;
    char *new_ptr; 

    size = 10;
    new_size = 11;
    ptr = new char[size];
    new_ptr = loc::resize(ptr, size, new_size);
    delete []new_ptr;

    return EXIT_SUCCESS;
}
