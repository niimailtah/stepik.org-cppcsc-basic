#include <iostream>

using namespace std;

namespace loc {
char *getline()
{
    char c;
    char *line;
    char *new_line;
    size_t quant;
    size_t total_size, current_size;

    quant = 16;
    current_size = 0;
    total_size = quant;
    line = new char[total_size + 1];
    while (cin.get(c))
    {
        if (c == '\n')
        {
            break;
        }
        if (current_size > total_size)
        {
            total_size += quant;
            new_line = new char[total_size + 1];
            for (size_t i = 0; i < current_size; ++ i)
            {
                new_line[i] = line[i];
                new_line[i + 1] = '\0';
            }
            delete [] line;
            line = new_line;
        }
        line[current_size] = c;
        line[current_size + 1] = '\0';
        current_size++;
    }

    return line;
}
} // namespace loc

int main()
{
    char *result;

    result = loc::getline();
    cout << result << endl;

    return EXIT_SUCCESS;
}
