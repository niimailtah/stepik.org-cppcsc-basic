#include <iostream>

using namespace std;

#include <cstddef> // size_t
#include <cstring>

struct String
{
    explicit String(const char *str = "")
    {
        size = std::strlen(str);
        this->str = new char[size + 1];
        std::strcpy(this->str, str);
    }

    String(size_t n, char c)
    {
        size = n;
        str = new char[size + 1];
        std::memset(str, (int)c, size);
    }

    ~String()
    {
        delete[] str;
    }

    String(const String &other)
        : size(other.size)
    {
        str = new char[other.size + 1];
        std::strcpy(str, other.str);
    }

    // Воспользуемся материалами лекции
    void swap(String &other)
    {
        std::swap(size, other.size);
        std::swap(str, other.str);
    }
        
    String &operator=(const String &other)
    {
        if (this != &other)
        {
            String(other).swap(*this);
        }
        return *this;
    }

    struct Substring
    {
        explicit Substring(char *str, int pos)
            : substr(str + pos), begin_pos(pos)
        {
        }

        String operator[](int pos)
        {
            size = pos - begin_pos;
            char *tmp_str = new char[size + 1];
            std::strncpy(tmp_str, substr, size);
            tmp_str[size] = '\0';
            String res(tmp_str);
            delete[] tmp_str;
            
            return res;
        }

        char   *substr;
        int    begin_pos;
        size_t size;
    };
    
    Substring operator[](int pos) const
    {
        return Substring(this->str, pos);
    }

    size_t size;
    char *str;
};

int main()
{
    String hello("hello");
    String hell = hello[0][4]; // теперь в hell хранится подстрока "hell"
    String ell  = hello[1][4]; // теперь в ell хранится подстрока "ell"
    std::cout << hello.str << std::endl;
    std::cout << hello[1][2].str << std::endl;
    std::cout << hell.str << std::endl;
    std::cout << ell.str << std::endl;

    return EXIT_SUCCESS;
}
