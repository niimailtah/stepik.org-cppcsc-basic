#include <iostream>

using namespace std;

struct Rational
{
    Rational(int numerator = 0, int denominator = 1)
        : numerator_(numerator), denominator_(denominator)
    {};

    void add(Rational rational)
    {
        numerator_ = numerator_ * rational.denominator_ + rational.numerator_ * denominator_;
        denominator_ *= rational.denominator_;
    }
    
    void sub(Rational rational)
    {
        numerator_ = numerator_ * rational.denominator_ - rational.numerator_ * denominator_;
        denominator_ *= rational.denominator_;
    }

    void mul(Rational rational)
    {
        numerator_ *= rational.numerator_;
        denominator_ *= rational.denominator_;
    }

    void div(Rational rational)
    {
        numerator_ *= rational.denominator_;
        denominator_ *= rational.numerator_;
    }

    void neg()
    {
        numerator_ = -numerator_;
    }

    void inv()
    {}

    double to_double() const
    {
        return numerator_ / (double) denominator_;
    }

    Rational operator+() const
    {
        Rational r(this->numerator_, this->denominator_);
        return r;
    }

    Rational operator-() const
    {
        Rational r(this->numerator_, this->denominator_);
        r.neg();
        return r;
    }

    Rational & operator+=(Rational rational)
    {
        this->add(rational);
        return *this;
    }

    Rational & operator-=(Rational rational)
    {
        this->sub(rational);
        return *this;
    }

    Rational & operator*=(Rational rational)
    {
        this->mul(rational);
        return *this;
    }

    Rational & operator/=(Rational rational)
    {
        this->div(rational);
        return *this;
    }

private:
    int numerator_;
    int denominator_;
};

Rational operator+(Rational lhs, Rational rhs)
{
    lhs += rhs;
    return lhs;
}

Rational operator-(Rational lhs, Rational rhs)
{
    lhs -= rhs;
    return lhs;
}

Rational operator*(Rational lhs, Rational rhs)
{
    lhs *= rhs;
    return lhs;

}

Rational operator/(Rational lhs, Rational rhs)
{
    lhs /= rhs;
    return lhs;
}

int main()
{
    Rational r1(1, 2);
    Rational r2(1, 3);
    Rational r3(5);
    Rational res;

    res = r1 + r2;
    cout << r1.to_double() << " + " << r2.to_double() << " = " << res.to_double() << endl;
    res = 10 + r2;
    cout << 10 << " + " << r2.to_double() << " = " << res.to_double() << endl;
    res = r2 + 10;
    cout << r2.to_double() << " + " << 10 << " = " << res.to_double() << endl;
    res = r1 - r2;
    cout << r1.to_double() << " - " << r2.to_double() << " = " << res.to_double() << endl;
    res = r2 * r1;
    cout << r2.to_double() << " * " << r1.to_double() << " = "  << res.to_double() << endl;
    res = r3 / r2;
    cout << r3.to_double() << " / " << r2.to_double() << " = "  << res.to_double() << endl;

    return EXIT_SUCCESS;
}
