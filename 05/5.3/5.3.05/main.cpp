#include <iostream>

using namespace std;

struct Expression;
struct Number;
struct BinaryOperation;

// -----------------------------------------------------------------------------
// class Expression
//
struct Expression
{
    virtual ~Expression() {};
    virtual double evaluate() const = 0;
};

// -----------------------------------------------------------------------------
// class Number
//
struct Number : Expression
{
    Number(double value)
        : value(value)
    {}

    double evaluate() const
    {
        return value;
    }

private:
    double value;
};

// -----------------------------------------------------------------------------
// class BinaryOperation
//
struct BinaryOperation : Expression
{
    BinaryOperation(Expression const * left, char op, Expression const * right)
        : left(left), op(op), right(right)
    {}

    ~BinaryOperation()
    {
        delete left;
        delete right;
    }

    double evaluate() const
    {
        double value;

        switch (op)
        {
            case '+':
                value = left->evaluate() + right->evaluate();
                break;
            case '-':
                value = left->evaluate() - right->evaluate();
                break;
            case '*':
                value = left->evaluate() * right->evaluate();
                break;
            case '/':
                value = left->evaluate() / right->evaluate();
                break;
        }
        return value;
    }

private:
    Expression const * left;
    Expression const * right;
    char op;
};

// =============================================================================
struct TRefCounter
{
    TRefCounter()
        : m_counter(0)
    {
    };
 
    TRefCounter(const TRefCounter&) = delete;
    TRefCounter& operator=(const TRefCounter&) = delete;
 
    ~TRefCounter()
    {
    }
 
    void reset()
    {
        m_counter = 0;
    }
 
    unsigned int get()
    {
        return m_counter;
    }
 
    void operator++()
    {
        m_counter++;
    }
 
    void operator++(int)
    {
        m_counter++;
    }
 
    void operator--()
    {
        m_counter--;
    }

    void operator--(int)
    {
        m_counter--;
    }
 
private:
    unsigned int m_counter{};
};

// =============================================================================
struct SharedPtr
{
    explicit SharedPtr(Expression *ptr = 0)
    {
        m_ptr = ptr;
        m_counter = new TRefCounter();
        if (ptr != NULL)
        {
            (*m_counter)++;
        }
    }

    SharedPtr(const SharedPtr &other)
    {
        if (this != &other)
        {
            m_ptr = other.m_ptr;
            m_counter = other.m_counter;
            if (other.m_ptr != NULL)
            {
                (*m_counter)++;
            }
        }
    }

    void destroy()
    {
        if (isvalid())
        {
            (*m_counter)--;
            if (m_counter->get() == 0)
            {
                delete m_ptr;
                delete m_counter;
            }
            m_ptr = NULL;
            m_counter = NULL;
        }   
    }
    
    ~SharedPtr()
    {
        destroy();
    }

    SharedPtr& operator=(const SharedPtr &other)
    {
        if (this != &other)
        {
            if (m_ptr != NULL)
            {
                (*m_counter)--;
            }
            if (other.m_ptr != NULL)
            {
                (*m_counter)++;
            }
            m_ptr = other.m_ptr;
        }
        return *this;
    }

    void reset(Expression *ptr = 0)
    {
        (*m_counter)--;
        m_ptr = ptr;
        if (m_counter->get() == 0)
        {
            delete m_ptr;
            m_ptr = NULL;
        }
    }

    Expression& operator*() const
    {
        return *m_ptr;
    }

    Expression* get() const
    {
        return m_ptr;
    }

    Expression* operator->() const
    {
        return m_ptr;
    }

    bool isvalid() const
    {
        return (m_ptr != NULL && m_counter != NULL);
    }

private:
    TRefCounter *m_counter;
    Expression *m_ptr;
};

int main()
{
    Expression *num1 = new Number(1.0);
    Expression *num2 = new Number(2.0);
    SharedPtr p1(num1);
    SharedPtr p2 = p1;
    Expression *e = p2.get();

    // // ptr1 pointing to an integer.
    // Shared_ptr<int> ptr1(new int(151));
    // cout << "--- Shared pointers ptr1 ---\n";
    // *ptr1 = 100;
    // cout << " ptr1's value now: " << *ptr1 << endl;
    // cout << ptr1;
 
    // {
    //     // ptr2 pointing to same integer
    //     // which ptr1 is pointing to
    //     // Shared pointer reference counter
    //     // should have increased now to 2.
    //     Shared_ptr<int> ptr2 = ptr1;
    //     cout << "--- Shared pointers ptr1, ptr2 ---\n";
    //     cout << ptr1;
    //     cout << ptr2;
 
    //     {
    //         // ptr3 pointing to same integer
    //         // which ptr1 and ptr2 are pointing to.
    //         // Shared pointer reference counter
    //         // should have increased now to 3.
    //         Shared_ptr<int> ptr3(ptr2);
    //         cout << "--- Shared pointers ptr1, ptr2, ptr3 "
    //                 "---\n";
    //         cout << ptr1;
    //         cout << ptr2;
    //         cout << ptr3;
    //     }
 
    //     // ptr3 is out of scope.
    //     // It would have been destructed.
    //     // So shared pointer reference counter
    //     // should have decreased now to 2.
    //     cout << "--- Shared pointers ptr1, ptr2 ---\n";
    //     cout << ptr1;
    //     cout << ptr2;
    // }
 
    // // ptr2 is out of scope.
    // // It would have been destructed.
    // // So shared pointer reference counter
    // // should have decreased now to 1.
    // cout << "--- Shared pointers ptr1 ---\n";
    // cout << ptr1;
 
    return EXIT_SUCCESS;
}
