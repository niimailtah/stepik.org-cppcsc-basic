#include <iostream>

using namespace std;

struct Expression;
struct Number;
struct BinaryOperation;

// -----------------------------------------------------------------------------
// class Expression
//
struct Expression
{
    virtual ~Expression() {};
    virtual double evaluate() const = 0;
};

// -----------------------------------------------------------------------------
// class Number
//
struct Number : Expression
{
    Number(double value)
        : value(value)
    {}

    double evaluate() const
    {
        return value;
    }

private:
    double value;
};

// -----------------------------------------------------------------------------
// class BinaryOperation
//
struct BinaryOperation : Expression
{
    BinaryOperation(Expression const * left, char op, Expression const * right)
        : left(left), op(op), right(right)
    {}

    ~BinaryOperation()
    {
        delete left;
        delete right;
    }

    double evaluate() const
    {
        double value;

        switch (op)
        {
            case '+':
                value = left->evaluate() + right->evaluate();
                break;
            case '-':
                value = left->evaluate() - right->evaluate();
                break;
            case '*':
                value = left->evaluate() * right->evaluate();
                break;
            case '/':
                value = left->evaluate() / right->evaluate();
                break;
        }
        return value;
    }

private:
    Expression const * left;
    Expression const * right;
    char op;
};

// =============================================================================
struct ScopedPtr
{
    explicit ScopedPtr(Expression *ptr = 0)
        : ptr_(ptr)
    {
    }

    ~ScopedPtr()
    {
        delete ptr_;
    }

    Expression* get() const
    {
        return ptr_;
    }

    Expression* release()
    {
        Expression * ptr = ptr_;
        this->ptr_ = NULL;
        return ptr;
    }

    void reset(Expression *ptr = 0)
    {
        delete ptr_;
        ptr_ = ptr;
    }

    Expression& operator*() const
    {
        return *ptr_;
    }

    Expression* operator->() const
    {
        return ptr_;
    }

private:
    // запрещаем копирование ScopedPtr
    ScopedPtr(const ScopedPtr&);
    ScopedPtr& operator=(const ScopedPtr&);

    Expression *ptr_;
};

int main()
{
    Expression *num1 = new Number(1.0);
    Expression *num2 = new Number(2.0);
    Expression *expr = new BinaryOperation(num1, '+', num2);
    ScopedPtr ptr(expr);

    return EXIT_SUCCESS;
}
