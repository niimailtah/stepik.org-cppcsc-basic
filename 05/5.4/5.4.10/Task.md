Ключевые слова static и inline
============================

В заголовочном файле `foo.hpp` есть определение функции:

```cpp
void foo(int i)
{
    std::cout << "i = " << i << std::endl;
}
```

В программе есть три корректных файла с кодом `first.cpp`, `second.cpp` и `third.cpp`, которые подключают `foo.hpp`.
